// Firmware for Teensy 5 panel dance pad with WS2812B RGB lights using the FastLED library
// author: Koen van Vliet
// version: 01
// date: 2018/03/14
// project: Original 5 panel dance pad using teensy

// Original Author's notes
// Teensy++ pin 6 (LED), may need 1k resistor pullup

/******************************************************************************/

/*** BEGIN Includes ***/
#include <Bounce.h>
#include <FastLED.h>
/*** END Includes ***/

/******************************************************************************/

/*** BEGIN Constants ***/
/* WS2812B LED strips config
 * Each tile has the same number of LEDs under it, in this case 6. There are 5 tiles in total. */
constexpr int kLightsPerTile = 6;
constexpr int kNumLeds = kLightsPerTile * 5;
constexpr int kLightDataPin = 21;

/* Set pin numbers of switches in the tiles */
constexpr int kPinSwitchTL = 16;
constexpr int kPinSwitchTR = 17;
constexpr int kPinSwitchCC = 18;
constexpr int kPinSwitchBL = 19;
constexpr int kPinSwitchBR = 20;
constexpr int kPinSwitchCOM = 23;

/* Colors config */
constexpr int kColorRed = 0x330000 * 4;
constexpr int kColorCyan = 0x001111 * 4;
constexpr int kColorOrange = 0x111100 * 4;

/* Switch debounce time in ms */
constexpr int kBounceDelay = 40;
/*** END Constants ***/

/******************************************************************************/

/*** BEGIN Global Variables ***/
/* Create an array for holding each LED's color information*/
CRGB leds[kNumLeds];

/* Create a debounce object for every switch */
Bounce switch_top_left = Bounce(kPinSwitchTL, kBounceDelay);
Bounce switch_top_right = Bounce(kPinSwitchTR, kBounceDelay);
Bounce switch_center = Bounce(kPinSwitchCC, kBounceDelay);
Bounce switch_bottom_left = Bounce(kPinSwitchBL, kBounceDelay);
Bounce switch_bottom_right = Bounce(kPinSwitchBR, kBounceDelay);

/* Flags for managing LED updating */
bool change_leds = false; // Set to true to update WS2812B with new values from leds[]. This flag is cleared automatically.
bool enable_leds = true; 
/*** END Global Variables ***/

/******************************************************************************/

/*** BEGIN Function Prototypes ***/
void SetTileColor(int tile, CRGB color);
void _SetTileColor(int tile, CRGB color);
/*** END Function Prototypes ***/

/******************************************************************************/

/*** Begin Function Implementations ***/

/* setup() : Set up pins and LEDs and perform self test LED pattern. */
void setup() {
  /* Set up GPIO for switch pins. */
  pinMode(kPinSwitchTL, INPUT_PULLUP);
  pinMode(kPinSwitchTR, INPUT_PULLUP);
  pinMode(kPinSwitchCC, INPUT_PULLUP);
  pinMode(kPinSwitchBL, INPUT_PULLUP);
  pinMode(kPinSwitchBR, INPUT_PULLUP);
  pinMode(kPinSwitchCOM, OUTPUT);
  digitalWrite(kPinSwitchCOM, LOW);

  /* Init FastLED library */
  FastLED.addLeds<WS2812B, kLightDataPin, GRB>(leds, kNumLeds);
  
  /* Quick power up self test of the LEDs (Cycle through all subpixels) */
  delay(500);
  fill_solid(leds, kNumLeds, CRGB::Red);
  FastLED.show();
  delay(1500);
  fill_solid(leds, kNumLeds, CRGB::Green);
  FastLED.show();
  delay(1500);
  fill_solid(leds, kNumLeds, CRGB::Blue);
  FastLED.show();
  delay(1500);
  fill_solid(leds, kNumLeds, CRGB::Black);
  FastLED.show();

  /* Disable LEDs if center panel is pressed while initializing. */
  if (digitalRead(kPinSwitchCC) == 0) {
    enable_leds = false;
  }
}

/* loop() : Read buttons, forward to usb joystick and control the LEDs. */
void loop() {
  // Update all the buttons.  There should not be any long
  // delays in loop(), so this runs repetitively at a rate
  // faster than the buttons could be pressed and released.
  switch_top_left.update();
  switch_top_right.update();
  switch_center.update();
  switch_bottom_left.update();
  switch_bottom_right.update();

  /* Check for switch close */
  if (switch_center.fallingEdge()) {
    Joystick.button(1,1);
    SetTileColor(2, kColorOrange);
  }
  if (switch_top_left.fallingEdge()) {
    Joystick.button(2,1);
    SetTileColor(0, kColorRed);
  }
  if (switch_top_right.fallingEdge()) {
    Joystick.button(3,1);
    SetTileColor(4, kColorRed);
  }
  if (switch_bottom_left.fallingEdge()) {
    Joystick.button(4,1);
    SetTileColor(1, kColorCyan);
  }
  if (switch_bottom_right.fallingEdge()) {
    Joystick.button(5,1);
    SetTileColor(3, kColorCyan);
  }

  /* Check for switch release */
  if (switch_center.risingEdge()) {
    Joystick.button(1,0);
    SetTileColor(2, CRGB::Black);
  }
  if (switch_top_left.risingEdge()) {
    Joystick.button(2,0);
    SetTileColor(0, CRGB::Black);
  }
  if (switch_top_right.risingEdge()) {
    Joystick.button(3,0);
    SetTileColor(4, CRGB::Black);
  }
  if (switch_bottom_left.risingEdge()) {
    Joystick.button(4,0);
    SetTileColor(1, CRGB::Black);
  }
  if (switch_bottom_right.risingEdge()) {
    Joystick.button(5,0);
    SetTileColor(3, CRGB::Black);
  }
  
  /* Update the LEDs when a switch was closed or released. */
  if (change_leds && enable_leds)
  {
    change_leds = false;  // Clear flag
    FastLED.show();  
  }
}

inline void SetTileColor(int tile, CRGB color) {
  _SetTileColor(tile, color);
  change_leds = true;
}

void _SetTileColor(int tile, CRGB color) {
  int i;
  for(i = 0; i < kLightsPerTile; ++i)
  {
    leds[tile * kLightsPerTile + i] = color;
  }
}
/*** END Function Implementations ***/
