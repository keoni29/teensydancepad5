### Five panel dance pad
I have created a DIY dance pad for playing dancing games on my computer. It uses a Teensy LC in it. This repository contains the sourcecode for this dancepad. The dance pad has 5 tiles, but this code can be modified to support any amount of tiles. Each tile has some WS2812B LEDs under it, which are controlled by the Teensy. Upon pressing the switches, the LEDS under the tile light up.


**Make sure to set your teensy USB type to Keyboard+Mouse+Joystick, otherwise it
will _not_ compile!** The Teensy will show up as a USB joystick.